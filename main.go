package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

type (
	action func()

	command struct {
		name   string
		action action
	}

	commandApplication struct {
		names         []string
		nameActionMap map[string]action
	}
)

func (a *commandApplication) Add(name string, action action) {
	if _, ok := a.nameActionMap[name]; ok {
		log.Fatalf("command %s already defined", name)
	}

	a.names = append(a.names, name)
	a.nameActionMap[name] = action
}

func (a *commandApplication) Run(name string) {
	if action, ok := a.nameActionMap[name]; ok {
		action()
	} else {
		a.help()

		log.Fatalf("undefined command %s", name)
	}
}

func (a *commandApplication) Close() error {
	return nil
}

func (a *commandApplication) help() {
	fmt.Println(`Example of command line interface.
Usage:

        go run <command>

The commands are:

        ` + strings.Join(a.names, "\n        ") + "\n")
}

func newCommandApplication(commands ...command) *commandApplication {
	result := &commandApplication{
		names:         make([]string, 0, len(commands)),
		nameActionMap: make(map[string]action, len(commands)),
	}

	result.Add("help", result.help)

	for _, c := range commands {
		result.Add(c.name, c.action)
	}

	return result
}

func main() {
	application := newCommandApplication(
		command{
			"senseye:example",
			func() {},
		},
	)
	defer application.Close()

	args := os.Args
	if len(args) < 2 {
		application.help()

		return
	}

	commandName := args[1]
	application.Run(commandName)
}
